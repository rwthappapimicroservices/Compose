#!/usr/bin/env sh
commit_sha="$CI_COMMIT_SHA"
# projectname=$(basename "$(pwd)" | tr "[:upper:]" "[:lower:]")
projectname="compose_${commit_sha}"
prefix="${projectname}_"

export COMPOSE_PROJECT_NAME="$projectname"  # magic variable for docker-compose

# compose files
graylog_compose="GrayLog/docker-compose.yml"
main_compose="docker-compose-testing.yml"
int_compose="docker-compose-inttest.yml"

print_tags()
{
    # XXX: add new variables here
    echo "Moodle Tag:             ${MOODLE_TAG:-not set}"
    echo "Moodle Environment Tag: ${MOODLE_ENV_TAG:-not set}"
    echo "Semester Tag:           ${SEMESTER_TAG:-not set}"
}

stop_all()
{
    # stop all
    docker-compose -f "$int_compose" --project-directory . down -v
    docker-compose -f "$main_compose" --project-directory . down -v
    docker-compose -f "$graylog_compose" --project-directory . down -v
}

start_env()
{
    # start environment
    docker-compose -f "$graylog_compose" --project-directory . pull; ret=$?
    test "$ret" -eq 0 || return "$ret"  # return if error
    docker-compose -f "$graylog_compose" --project-directory . up -d; ret=$?
    test "$ret" -eq 0 || return "$ret"  # return if error
    docker-compose -f "$main_compose" --project-directory . pull; ret=$?
    test "$ret" -eq 0 || return "$ret"  # return if error
    docker-compose -f "$main_compose" --project-directory . up -d; ret=$?
    test "$ret" -eq 0 || return "$ret"  # return if error
    docker-compose -f "$int_compose" --project-directory . pull; ret=$?
    test "$ret" -eq 0 || return "$ret"  # return if error
    docker-compose -f "$int_compose" --project-directory . up --no-start; ret=$?
    return "$ret"
}

start_tests()
{
    # start tests
    services=$(docker-compose -f "$int_compose" config --services)
    echo "$services" | xargs -I @ docker-compose -f "$int_compose" --project-directory . run @
    ret=$?
    return $ret
}


main()
{
    # stop old containers if they exist
    stop_all
    start_env; ret=$?                   # start environment
    test "$ret" -eq 0 || return "$ret"  # return if error
    start_tests; ret=$?                 # start tests
    return "$ret"
}

print_tags
main; ret=$?
stop_all
exit $ret
